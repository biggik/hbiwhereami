function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

(function($, console, doc) {
	var employeeViewModel,
        statusViewModel,
        _serviceURL;
    
    _serviceURL = "http://whereami.hbi.is/api/status";
        
	 employeeViewModel = kendo.observable({
		employees: new kendo.data.DataSource({
                //offlineStorage: "products-offline",
                transport: {
                    read: {
                        //contentType: "application/json; charset=utf-8",
                        type: "GET",
                        //data: "{}",
                        dataType: "json",
                        url: _serviceURL
                    }
                },
                change: function(e) {
                  //var data = this.data();
                  //console.log(data.length); // displays "77"
                  //console.log(this.data());
                },
                error: function(e) {
                  //var data = this.data();
                  //console.log(data.length); // displays "77"
                  //  console.log('In error event');
                },
                requestEnd: function(e) {
                  //var response = e.response;
                  //var type = e.type;
                  //console.log('in requestEnd: ' + type); // displays "read"
                  //console.log(response.length); // displays "77"
                }
            })
    });
    
     statusViewModel = kendo.observable({   
        employee: "",
        init: function(id) {        
            //console.log('in init');
            var that = this,
                dataSource;         
            kendo.data.ObservableObject.fn.init.apply(that, []);
           
            var dataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                         url: function(data) {
                               return _serviceURL + '/details/' + data.id;
                            },
                        type: "GET",
                        dataType: "json"
                    }
                },
                change: function(e) {
                  //var data = this.data();
                  //console.log(data.length); // displays "77"
                    console.log('In change event');
                    console.log(this.data());
                },
                error: function(e) {
                  //var data = this.data();
                  //console.log(data.length); // displays "77"
                    console.log('In error event');
                },
                requestEnd: function(e) {
                  var response = e.response;
                  var type = e.type;
                  console.log('in requestEnd: ' + type); // displays "read"
                    console.log(response);
                  //console.log(response.length); // displays "77"
                }
            });
            dataSource.read({ id: id });
            that.set("employee", dataSource);
            //console.log('in init3');
       }      
    });

    /*
    statusViewModel = kendo.observable({
        employee: new kendo.data.DataSource({
                transport: {
                    read: {
                        //contentType: "application/json; charset=utf-8",
                        type: "GET",
                        //data: "{}",
                        dataType: "json",
                        url: function(data) {
                            console.log('In status view model read');
                           return _serviceURL + '/details/' + data.id;
                        }
                    }
                },
                change: function(e) {
                  //var data = this.data();
                  //console.log(data.length); // displays "77"
                    console.log('In change event');
                    console.log(this.data());
                },
                error: function(e) {
                  //var data = this.data();
                  //console.log(data.length); // displays "77"
                    console.log('In error event');
                },
                requestEnd: function(e) {
                  var response = e.response;
                  var type = e.type;
                  console.log('in requestEnd: ' + type); // displays "read"
                    console.log(response);
                  //console.log(response.length); // displays "77"
                }
            })
    });*/
  
	$.extend(window, {
        employeeViewModel: employeeViewModel,
        statusViewModel: statusViewModel
	});
    
})(jQuery, console, document);