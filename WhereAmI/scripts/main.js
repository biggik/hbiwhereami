document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
    navigator.splashscreen.hide();
}

function formatDateOnly(date, includeYear) {
    var monthNames = [ 
        "jan", "feb", "mar", "apr", "maí", "jún",
        "júl", "ágú", "sep", "okt", "nóv", "des" 
    ];
	
    var s = date.getDate() + ". " + monthNames[date.getMonth()];
    if (includeYear) {
        s = s + "." + date.getFullYear();
    }
    return s;
}

function formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes;

    var today = new Date();
    var todayString = formatDateOnly(today, false);
    var dateString = formatDateOnly(date, false);
  
    if (todayString === dateString) {
        return strTime;
    }
  
    var i = 0;
    if (today.getFullYear() == date.getFullYear()) {
        return formatDateOnly(date, false) + " @ " + strTime;
    } else {
        return formatDateOnly(date, true) + " @ " + strTime;
    }
}

(function($, doc) {
    var _app,
        _isOnline = true;
    
    _app = {
        init: function() {
        },
        
        onAddCardViewShow: function () {
            addCardViewModel.resetView();
        },  
        statusViewInit: function (e) {
            console.log('in view init');
        },
        statusViewShowx: function () {
            console.log('in view show');
            console.log(getUrlVars()['id']);
        },
        
        statusViewShow: function(e) {
            var id = e.view.params.id;
            statusViewModel.init(id);
        }
    };
    
    _app.init();
        
    $.extend(window, {
                 employeeViewModel: _app.employeeViewModel,
                 statusViewModel: _app.statusViewModel,
                 statusViewShow: _app.statusViewShow
             });
}(jQuery, document));